import 'package:flutter_test/flutter_test.dart';
import 'package:myplan/models/plan.dart';
import 'package:myplan/services/service.dart';

void main() {
  test('Test create plan', () {
    Service service = Service();

    service.createPlan('Create screen to view details of product.');

    var allPlans = service.getAllPlans();
    expect(1, allPlans.length);
  });

  test('Test create addTask', () {
    Service service = Service();

    var plan = Plan(id: 1);
    var description = 'Try to make a screen to display product';
    service.createTask(plan, description);
  });
}
