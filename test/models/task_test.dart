import 'package:flutter_test/flutter_test.dart';
import 'package:myplan/models/common_model.dart';
import 'package:myplan/models/task.dart';

void main() {
  test('New task from Model', () {
    var model = Model(id: 1, data: {
      'description': 'Create a flutter app same as Tiki.',
      'isComplete': false
    });

    var task = Task.fromModel(model);

    expect(task.id, 1);
    expect(task.description, 'Create a flutter app same as Tiki.');
    expect(task.isComplete, false);
  });

  test('New task from Constructor', () {
    Task task = Task(
        id: 1,
        description: 'Create a flutter app same as Tiki.',
        isComplete: true);

    expect(task.id, 1);
    expect(task.description, 'Create a flutter app same as Tiki.');
    expect(task.isComplete, true);
  });
}
