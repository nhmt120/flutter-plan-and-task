import 'package:myplan/models/common_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Test Model', () {
    Model model = Model(id: 1, data: {'name': 'Finish all assignments'});
    expect(model.id, 1);
    expect(model.data.isEmpty, false);
    expect(model.data['name'], 'Finish all assignments');
  });
}
