import 'package:myplan/models/common_model.dart';
import 'package:myplan/models/plan.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:myplan/models/task.dart';

void main() {
  Plan plan = Plan(id: 1, name: 'Hello Plan');
  test('Test Plan model', () {
    expect(plan.id, 1);
    expect(plan.name, 'Hello Plan');
    expect(plan.tasks.isEmpty, true);

    expect(plan.completeCount, 0);
    expect(plan.completenessMessage, '0 out of 0 tasks');
  });

  test('Test Plan\'s constructors', () {
    var model = Model(id: 1);

    var plan = Plan.fromModel(model);
    expect(plan.id, 1);
    expect(1, plan.toModel().id);
  });

  test('Test Plan\'s completenessMessage', () {
    var model = Model(id: 1);
    var plan = Plan.fromModel(model);
    plan.tasks.add(Task(id: 1));
    plan.tasks.add(Task(id: 2));

    expect(plan.id, 1);
    expect(1, plan.toModel().id);
    expect(plan.completeCount, 0);
    expect(plan.completenessMessage, '0 out of 2 tasks');
  });
}
