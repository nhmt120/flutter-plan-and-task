import 'package:myplan/models/common_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:myplan/repositories/in_memory_cache.dart';
import 'package:myplan/repositories/repository.dart';

void main() {
  Repository repo = InMemoryCache();
  test('Test InMemory Repository', () {
    expect(repo.getAll().isEmpty, true);

    Model newModel = repo.create();
    expect(newModel.id, 1);

    expect(repo.getAll().isEmpty, false);

    Model? existedModel = repo.get(1);

    expect(newModel, existedModel);

    Model newModel2 = repo.create();
    expect(newModel2.id, 2);
    expect(repo.getAll().length, 2);
    expect(repo.getAll()[1], newModel2);
  });
}
