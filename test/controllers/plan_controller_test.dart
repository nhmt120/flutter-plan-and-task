import 'package:flutter_test/flutter_test.dart';
import 'package:myplan/controllers/plan_controller.dart';


void main() {
  test('Test createPlan', () {
    var controller = PlanController();
    controller.createPlan('A');

    expect(1, controller.plans.length);
    expect('A', controller.plans.first.name);
  });

  test('Test createPlan with Vietnamese name', () {
    var controller = PlanController();
    controller.createPlan('Tạo ứng dụng Flutter tương tự như Tiki');

    expect(1, controller.plans.length);
    expect(
        'Tạo ứng dụng Flutter tương tự như Tiki', controller.plans.first.name);
  });

  test('Test createPlan with duplicate name', () {
    var controller = PlanController();
    controller.createPlan('Hello Plan');
    controller.createPlan('Hello Plan');

    expect(2, controller.plans.length);
    expect('Hello Plan (2)', controller.plans.last.name);
  });
}
