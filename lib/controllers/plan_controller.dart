import 'package:myplan/models/plan.dart';
import 'package:myplan/models/task.dart';
import 'package:myplan/services/service.dart';

class PlanController {
  final Service _service = Service();

  // This public getter cannot be modified by any other object
  // (kinda like private, but still accessible static, you can see it but no operation allowed)
  List<Plan> get plans => List.unmodifiable(_service.getAllPlans());

  void createPlan(String name) {
    if (name.isEmpty) {
      return;
    }
    final allPlanNames = plans.map((plan) => plan.name).toList();
    _service.createPlan(checkDuplicated(allPlanNames, name));
  }

  void savePlan(Plan plan) {
    _service.savePlan(plan);
  }

  void deletePlan(Plan plan) {
    _service.deletePlan(plan);
  }

  String checkDuplicated(Iterable<String> names, String newName) {
    final duplicatedCount =
        names.where((name) => name.contains(newName, 0)).length;
    if (duplicatedCount > 0) {
      // Problem: not very good since if we suggest newName2 maybe, but the client rename it to newName3,
      // then another newName comes and we will suggest another newName3 which makes it duplicated again maybe causing a weird loop...?
      newName += ' (${duplicatedCount + 1})';
      return newName;
      // Solution: scan the number too and fill in the missing number sequentially
      // since we are already scanning through the list using where, I guess it won't affect the performance that much
      // not doing it yet :)
    }
    return newName;
  }

  void createTask(Plan plan, String description) {
    if (description.isEmpty) {
      description = 'New Task';
    }
    final descriptions = plan.tasks.map((task) => task.description).toList();
    _service.createTask(plan, checkDuplicated(descriptions, description));
  }

  void saveTask(Plan plan, Task task) {
    _service.saveTask(plan, task);
  }

  void deleteTask(Plan plan, Task task) {
    _service.deleteTask(plan, task);
  }
}
