import 'package:myplan/models/task.dart';

import 'common_model.dart';

class Plan {
  int id = 0;
  String name = '';
  List<Task> tasks = [];

  Plan({
    required this.id,
    this.name = '',
  });

  int get completeCount => tasks.where((task) => task.isComplete).length;

  String get completenessMessage =>
      '$completeCount out of ${tasks.length} tasks';

  Model toModel() {
    return Model(
      id: id,
      data: {
        'name': name,
        'tasks': tasks.map((task) => task.toModel()).toList(),
      },
    );
  }

  Plan.fromModel(Model model) {
    // this is a named constructor, different from the constructor above so the 'required' up there doesn't work here
    // which is why we have to make the class properties late or define its value up there in the first place.
    id = model.id;
    name = model.data['name'] ?? ''; // ??: if null then return this nothing string ''
    if (model.data['tasks'] != null) {
      tasks = model.data['tasks']
          .map<Task>((task) => Task.fromModel(task))
          .toList();
    }
  }
}
