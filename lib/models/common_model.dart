class Model {
  // Common model for Plan and Task model
  final int id;
  final Map data;

  Model({
    required this.id,
    this.data = const {},
  });
}