import 'common_model.dart';

class Task {
  late final int id;
  late String description;
  late bool isComplete;

  Task({
    required this.id,
    this.description = '',
    this.isComplete = false,
  });

  Task.fromModel(Model model): id = model.id,
        description = model.data['description'],
        isComplete = model.data['isComplete'];

  Model toModel() {
    return Model(
      id: id,
      data: {
        'description': description,
        'isComplete': isComplete,
      },
    );
  }
}
