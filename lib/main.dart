import 'package:flutter/material.dart';
import 'package:myplan/plan_provider.dart';
import 'package:myplan/views/app.dart';

void main() {
  var planProvider = PlanProvider(child: MyPlanApp());
  runApp(planProvider);
}
