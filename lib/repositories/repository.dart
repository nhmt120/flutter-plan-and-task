import 'package:myplan/models/common_model.dart';

abstract class Repository {
  // for both Plan and Task operations too since Model is the 'abstract' kind of combined type of Plan and Task.
  Model create();
  List<Model> getAll();

  Model? get(int id);

  void update(Model item);
  Model? delete(int id);
  void clear();
}