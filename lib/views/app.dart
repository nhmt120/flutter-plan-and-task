import 'package:flutter/material.dart';
import 'package:myplan/views/plan_creator_screen.dart';

class MyPlanApp extends StatelessWidget {
  const MyPlanApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My Plan',
      home: Scaffold(
        body: PlanCreatorScreen(),
      ),
    );
  }
}
