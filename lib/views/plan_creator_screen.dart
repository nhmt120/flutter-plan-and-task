import 'package:flutter/material.dart';
import 'package:myplan/controllers/plan_controller.dart';
import 'package:myplan/plan_provider.dart';
import 'package:myplan/views/plan_task_screen.dart';

class PlanCreatorScreen extends StatefulWidget {
  const PlanCreatorScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PlanCreatorScreenState();
  }
}

class PlanCreatorScreenState extends State<PlanCreatorScreen> {
  final textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Plan'),
      ),
      body: Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            buildPlanCreator(),
            const SizedBox(height: 10),
            buildPlanList(),
          ],
        ),
      ),
    );
  }

  void addPlan() {
    PlanController planController = PlanProvider.of(context);
    planController.createPlan(textController.text);
    textController.clear();
    setState(
      () {},
    );
  }

  Widget buildPlanCreator() {
    return Material(
      color: Theme.of(context).cardColor,
      elevation: 25,
      child: TextField(
        controller: textController,
        decoration: const InputDecoration(
          labelText: 'Create a new plan',
          contentPadding: EdgeInsets.all(20),
        ),
        onEditingComplete: addPlan,
      ),
    );
  }

  Widget buildPlanList() {
    final plans = PlanProvider.of(context).plans;

    return Expanded(
      child: ListView.builder(
        padding: const EdgeInsets.all(10.0),
        itemCount: plans.length,
        itemBuilder: (BuildContext context, int index) {
          final plan = plans[index];
          return Dismissible(
            key: ValueKey(plan),
            background: Container(color: Colors.red),
            direction: DismissDirection.endToStart,
            onDismissed: (_) {
              final controller = PlanProvider.of(context);
              controller.deletePlan(plan);
              setState(() {});
            },
            child: ListTile(
              title: Text(plan.name),
              subtitle: Text(plan.completenessMessage),
              onTap: () async {
                await Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => PlanTaskScreen(
                      plan: plan,
                    ),
                  ),
                );
                setState(() {});
              },
            ),
          );
        },
      ),
    );
  }
}
