import 'package:flutter/material.dart';
import 'package:myplan/models/plan.dart';
import 'package:myplan/models/task.dart';
import 'package:myplan/plan_provider.dart';

class PlanTaskScreen extends StatefulWidget {
  final Plan plan;

  const PlanTaskScreen({Key? key, required this.plan}) : super(key: key);

  @override
  State createState() => PlanTaskScreenState();
}

class PlanTaskScreenState extends State<PlanTaskScreen> {
  late ScrollController scrollController;

  Plan get plan => widget.plan;

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController()
      ..addListener(() {
        FocusScope.of(context).requestFocus(FocusNode());
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Plan ${plan.name}')),
      body: Column(
        children: <Widget>[
          Expanded(child: buildList()),
          SafeArea(child: Text(plan.completenessMessage)),
        ],
      ),
      floatingActionButton: buildAddTaskButton(),
    );
  }

  buildList() {
    return ListView.builder(
      controller: scrollController,
      itemCount: plan.tasks.length,
      itemBuilder: (context, index) => buildTaskTile(plan.tasks[index]),
    );
  }

  buildTaskTile(Task task) {
    final controller = PlanProvider.of(context); // this is the moved line
    return Dismissible(
      key: ValueKey(task),
      background: Container(color: Colors.red),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        // final controller = PlanProvider.of(context);
        controller.deleteTask(plan, task);
        setState(() {});
      },
      child: ListTile(
        leading: Checkbox(
            value: task.isComplete,
            onChanged: (selected) {
              setState(() {
                task.isComplete = selected as bool;
                controller.saveTask(plan, task);
              });
            }),
        title: TextFormField(
          initialValue: task.description,
          onFieldSubmitted: (text) {
            setState(() {
              task.description = text;
              controller.saveTask(plan, task);
            });
          },
        ),
      ),
    );
  }

  buildAddTaskButton() {
    return FloatingActionButton(
      child: const Icon(Icons.add),
      onPressed: () {
        final controller = PlanProvider.of(context);
        controller.createTask(plan,
            'New Task'); // stereotype only: but edit this, user should input the task description right after clicking the add button
        setState(() {});
      },
    );
  }
}
