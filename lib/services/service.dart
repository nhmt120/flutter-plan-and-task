import 'package:myplan/models/plan.dart';
import 'package:myplan/models/task.dart';
import 'package:myplan/repositories/in_memory_cache.dart';

class Service {
  // can make all these to static field and methods
  InMemoryCache _repository = InMemoryCache();

  void createPlan(String name) {
    final planModel = _repository.create();
    final plan = Plan.fromModel(planModel)..name = name;
    savePlan(plan);
    // return plan;
  }

  void savePlan(Plan plan) {
    // since the plan id is generated from the createPlan by the InMemory repo
    // so the update is gonna save it at the correct unique id
    _repository.update(plan.toModel());
  }

  void deletePlan(Plan plan) {
    _repository.delete(plan.id);
  }

  List<Plan> getAllPlans() {
    return _repository
        .getAll()
        .map<Plan>((plan) => Plan.fromModel(plan))
        .toList();
  }

  void createTask(Plan plan, String description) {
    final id = (plan.tasks.isEmpty) ? 1 : plan.tasks.last.id + 1;
    Task task = Task(id: id, description: description);
    plan.tasks.add(task);
    savePlan(plan);
  }

  void saveTask(Plan plan, Task task) {
    // also plays update task too
    // similar reasons to the above
    plan.tasks[task.id - 1] =
        task; // -1 because the task generated id from addTask starts at 1, the tasks index starts at 0
    _repository.update(plan.toModel());
  }

  void deleteTask(Plan plan, Task task) {
    plan.tasks.remove(task);
    savePlan(plan);
  }

  List<Task> getAllTask(Plan plan) {
    return plan.tasks;
  }
}
